﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class DtoEntrega
    {
        public int Id { get; set; }
        public string endereco { get; set; }
        public string cep { get; set; }
        public string ncliente { get; set; }
    }
}
