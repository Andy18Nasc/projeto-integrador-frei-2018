﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabaseCadastro
    {
        public int Salvar(DtoCadastro LoL)
        {
            string script =
            @"INSERT INTO tb_cadastro 
            (             
	            nm_nome, 
                ds_email,
                vl_idade,
                ds_senha
            )
            VALUES
            (
                @nm_nome,	               
                ds_email,   
                vl_idade,
                ds_senha
	            
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", LoL.nome));
            parms.Add(new MySqlParameter("ds_email", LoL.email));
            parms.Add(new MySqlParameter("vl_idade", LoL.idade));
            parms.Add(new MySqlParameter("ds_senha", LoL.senha));




            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }


        public bool Logar(string nome, string senha)
        {
            string script =

                @"select * 
                    from tb_cadastro
                    where nm_nome = @nm_nome
                    and ds_senha  = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", nome));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader read = db.ExecuteSelectScript(script, parms);

            if (read.Read())
            {
                return true;
            }
            else
            {
                return false;
            }


        }
    }

}