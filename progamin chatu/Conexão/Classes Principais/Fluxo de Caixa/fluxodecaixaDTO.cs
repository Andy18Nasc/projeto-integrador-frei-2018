﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Conexão.Classes_Principais.Fluxo_de_Caixa
{
    class fluxodecaixaDTO
    {
        public string data { get; set; }
        public int ganhos { get; set; }
        public int despesas { get; set; }
    }
}
