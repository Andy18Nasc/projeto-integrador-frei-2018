﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Conexão.Classes_Principais.Fluxo_de_Caixa
{
    class fluxodecaixaDATABASE
    {
        public fluxodecaixaDTO salvar(fluxodecaixaDTO sii)
        {
            string script = @"insert into tb_fluxodecaixa 
                            ( dt_data, vl_ganhos, vl_despesas)
                            Values
                            (@dt_data, @vl_ganhos, @vl_despesas)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_data", sii.data));
            parms.Add(new MySqlParameter("vl_ganhos", sii.ganhos));
            parms.Add(new MySqlParameter("vl_despesas", sii.despesas));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            fluxodecaixaDTO din = null;
            if (reader.Read())
            {
                din = new fluxodecaixaDTO();
                din.data = reader.GetString("dt_data");
                din.ganhos = reader.GetInt32("vl_ganhos");
                din.despesas = reader.GetInt32("vl_despesas");
            }
            reader.Close();
            return din;
        }

            public List<fluxodecaixaDTO> consultar(string data, int despesas, int ganhos)
            {

                string script =
                    @"SELECT * FROM tb_fluxodecaixa WHERE ds_data, vl_ganhos, vl_despesas like @ds_data, @vl_ganhos, @vl_despesas";
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("ds_data", "%" + data + "%"));
                parms.Add(new MySqlParameter("vl_despesas", "%" + despesas + "%"));
                parms.Add(new MySqlParameter("vl_ganhos", "%" + ganhos + "%"));
                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
                List<fluxodecaixaDTO> cap = new List<fluxodecaixaDTO>();
                while (reader.Read())
                {

                    fluxodecaixaDTO capital = new fluxodecaixaDTO();
                    capital.data = reader.GetString("dt_data");
                    capital.ganhos = reader.GetInt32("vl_ganhos");
                    capital.despesas = reader.GetInt32("vl_despesas");               
                    cap.Add(capital);

                }
                reader.Close();
                return cap;
            }
    }
}
