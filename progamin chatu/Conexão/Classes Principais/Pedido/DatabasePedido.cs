﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabasePedido
    {
        public int save(string bairro, int cpf, string cidade, int rg, string user)
        {
            string script = @"insert into TB_pedido(ds_cpf, ds_bairro, ds_rg, nm_cidade, ds_user) values (@ds_cpf, @ds_bairro, @ds_rg, @nm_cidade, @ds_user)";
            List<MySqlParameter> Bachellor = new List<MySqlParameter>();         
            Bachellor.Add(new MySqlParameter("ds_cpf", cpf));
            Bachellor.Add(new MySqlParameter("ds_bairro", bairro));
            Bachellor.Add(new MySqlParameter("ds_rg", rg));
            Bachellor.Add(new MySqlParameter("nm_cidade", cidade));
            Bachellor.Add(new MySqlParameter("ds_user", user));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, Bachellor);
        }
        
    
    }
}
