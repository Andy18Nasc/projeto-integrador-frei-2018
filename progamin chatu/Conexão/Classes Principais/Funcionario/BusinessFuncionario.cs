﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class BusinessFuncionario
    {
        public DtoFuncionario Salvar(DtoFuncionario cads)
        {
            if (cads.nome == string.Empty)
            {
                throw new ArgumentException("user obrigatorio");
            }
            if (cads.cargo == string.Empty)
            {
                throw new ArgumentException("a senha é obrigatoria");
            }

            DatabaseFuncionario db = new DatabaseFuncionario();
            return db.Salvar(cads);
        }

        DatabaseFuncionario db = new DatabaseFuncionario();
        public void Remover(int id)
        {
            db.Remover(id);
            
        }
        public void alterar(DtoFuncionario sen)
        {
            DatabaseFuncionario db = new DatabaseFuncionario();
            db.alterar(sen);
        }

        public List<DtoFuncionario> consultar(string nome, string cargo)
        {
            DatabaseFuncionario db = new DatabaseFuncionario();
            List<DtoFuncionario> tb = db.consultar(nome, cargo);

            return tb;
        }
        public List<DtoFuncionario> Listar()
        {
            DatabaseFuncionario db = new DatabaseFuncionario();
            List<DtoFuncionario> tb = db.Listar();

            return tb;
        }

    }
}
