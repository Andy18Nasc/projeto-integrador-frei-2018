﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabaseRoupas
    {
        public int salvar(DtoRoupas savler)
        {
            string script = @"Insert Into tb_produto (nm_categoria, ds_genero, vl_preco, vl_quantidade) Values (@nm_categoria, @ds_genero,@vl_preco, @vl_quantidade)";
            List<MySqlParameter> roma = new List<MySqlParameter>();
            roma.Add(new MySqlParameter("nm_categoria", savler.categoria));
            roma.Add(new MySqlParameter("ds_genero", savler.genero));
            roma.Add(new MySqlParameter("vl_preco", savler.preco));
            roma.Add(new MySqlParameter("vl_quantidade", savler.quantidade));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, roma);
        }
        public void Alterar(DtoRoupas roupas)
        {
            string script =
            @"UPDATE tb_produto set
                     nm_categoria  = @nm_categoria,
	                 ds_genero   = @ds_genero,
	            WHERE vl_preco   = @vl_preco,	                 
                ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", roupas.categoria));
            parms.Add(new MySqlParameter("vl_preco", roupas.preco));
            parms.Add(new MySqlParameter("ds_genero", roupas.genero));
            ;
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DtoRoupas> consultar (string produto)
        {
            string script =
            @"SELECT * 
                FROM tb_produto 
                    where ds_genero = @genero";
                   
               //WHERE id_produto like @id_produto  
                  //";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("genero", "%" + produto + "%"));
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            
            List<DtoRoupas> roupas = new List<DtoRoupas>();
            
            while (reader.Read())
            {
                DtoRoupas newroupa = new DtoRoupas();
                newroupa.categoria = reader.GetString("nm_categoria");
                newroupa.genero = reader.GetString("ds_genero");
                roupas.Add(newroupa);
            }
            reader.Close();

            return roupas;

        }
            public void remover(string genero, string categoria, int preco)
            {
                string script =
                    @"Delete From tb_produto Where nm_categoria, Where vl_preco, and ds_genero = @nm_categoria, @vl_preco, @ds_genero";
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("nm_categoria", categoria));
                parms.Add(new MySqlParameter("ds_genero", genero));
                parms.Add(new MySqlParameter("vl_preco", preco));
                Database db = new Database();
                db.ExecuteInsertScript(script, parms);
                
            }
        }
    }

