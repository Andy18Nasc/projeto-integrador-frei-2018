﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabasePedidoItem
    {
        public int saving(DtoPedidoItem dto)
        {
            string script = @"Insert into TB_pedidoItem (id_cliente, id_pedido) Values (@id_produto, @id_pedido)";
            List<MySqlParameter> ultra = new List<MySqlParameter>();
            ultra.Add(new MySqlParameter("id_cliente", dto.IDcliente));
            ultra.Add(new MySqlParameter("id_pedido", dto.IDpedido));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, ultra);
        }
    }
}
