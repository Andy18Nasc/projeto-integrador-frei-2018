﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Conexão.Classes_Principais.Folha_de_Pagamento
{
    public class FolhaDTO
    {
        public string nomeFuncionario { get; set; }
        public int Id { get; set; }
        public int IdFuncionario { get; set; }
        public double SalarioBase { get; set; }
        public double HE { get; set; }
        public double Convenio { get; set; }
        public double DSR { get; set; }
        public double VR { get; set; }
        public double VT { get; set; }
        public double SalarioFamilia { get; set; }
        public double Bonus { get; set; }
        public double Atraso { get; set; }
        public double Falta { get; set; }
        public double CestaBasica { get; set; }
        public double INSS { get; set; }
        public double IRRF { get; set; }
        public double FGTS { get; set; }
        public double SalarioLiquido { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public DtoFuncionario funcio { get; set; }

    }
}
