﻿using progamin_chatu.Atualizado;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class Finalizando : Form
    {
        public Finalizando()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(2000);
                Invoke(new Action(() =>
                {
                    Login frm = new Login();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void Finalizando_Load(object sender, EventArgs e)
        {

        }
    }
}
