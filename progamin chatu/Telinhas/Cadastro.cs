﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class Cadastro : Form
    {
        public Cadastro()
        {
            InitializeComponent();
        }

        private void Cadastro_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Login tela = new Login();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DtoCadastro cad = new DtoCadastro();
                cad.nome = txtnome.Text;
                cad.email = txtemail.Text;
                cad.idade = txtidade.Text;
                cad.senha = txtsenha.Text;


                BusinessCadastro business = new BusinessCadastro();
                business.Salvar(cad);

                EnviarMensagem("Cadastro feito com sucesso!");
            }
            catch (ArgumentException ex)
            {
                EnviarMensagemErro(ex.Message);
                
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro: " + ex.Message);
            }
           
        }


        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "GRstore",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "GRstore",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtidade_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
