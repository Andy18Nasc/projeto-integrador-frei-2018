﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class Pedido : Form
    {
        public Pedido()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DtoPedido pedido = new DtoPedido();
                pedido.user = txtuser.Text;
                pedido.cpf = txtcpf.Text;
                pedido.bairro = txtbairro.Text;
                pedido.cidade = txtcidade.Text;
                pedido.rg = txtrg.Text;

                BusinessPedido buss = new BusinessPedido();
                buss.save(pedido);

                EnviarMessage("Seu Pedido foi feito com Sucesso! Aguarde a Finalização");
            }
            catch (ArgumentException ex)
            {
                EnviarErro(ex.Message);

            }
            catch (Exception ex)
            {
                EnviarErro("ocorreu um erro ao salvar seu pedido: " + ex.Message);
            }
        }
            
            private void EnviarMessage(string message)
            {
                MessageBox.Show(message, "Girls Store", MessageBoxButtons.OK);
            }
            private void EnviarErro(string message)
            {
            MessageBox.Show(message, "Girls Store", MessageBoxButtons.OK);

            }

           

        }
    }

