﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class ConsultarFuncionario : Form
    {
        public ConsultarFuncionario()
        {
            InitializeComponent();
            

        }

        private void carregamentodegrid(DtoFuncionario func)
        {
            this.func = func;
            lblnm.Text = func.nome;
        }

        DtoFuncionario func;

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                func.nome = txtnomes.Text;


                BusinessFuncionario business = new BusinessFuncionario();
                business.alterar(func);  // outro erro chato demais //

                EnviarMensagem("Feito!");
            }
            catch (ArgumentException ex)
            {
                EnviarMensagemErro(ex.Message);
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro ao alterar: " + ex.Message);
            }
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "GRstore",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "GRstore",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void button3_Click(object sender, EventArgs e)
        {
           // LoadGrid();

            try
            {
                string nome = txtnomes.Text;
                string cargo = txtnomes.Text;

                BusinessFuncionario pass = new BusinessFuncionario();
                List<DtoFuncionario> list = pass.consultar(nome, cargo); //erro chato!// 
                gvfuncionarios.AutoGenerateColumns = false;
                gvfuncionarios.DataSource = list;

            }
            catch (Exception ex)
            {
                MessageBox.Show("erro!" + ex.Message);

            }
        }

        //public void LoadGrid()
        //{
        //    try
        //    {
        //        string nome = txtnomes.Text;
        //        string cargo = txtnomes.Text;
                
        //        BusinessFuncionario pass = new BusinessFuncionario();
        //        List<DtoFuncionario> list = pass.consultar(nome, cargo); //erro chato!// 
        //        gvfuncionarios.AutoGenerateColumns = false;
        //        gvfuncionarios.DataSource = list;

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("erro!" + ex.Message);
                
        //    }
        //}

        private void gvfuncionarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {           
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
    

