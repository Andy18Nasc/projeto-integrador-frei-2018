﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Principais.Fluxo_de_Caixa;
using progamin_chatu.Conexão.Classes_Principais.Outras;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class Fluxo_de_Caixar : Form
    {
        public Fluxo_de_Caixar()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string data = txtdata.Text;
                int ganhos = Convert.ToInt32(txtganhos.Text);
                int despesas = Convert.ToInt32(txtdespesas.Text);
                int total = ganhos - despesas;
                lbltotal.Text = total.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("erro" + ex.Message);
                
            }
        }

        private void enviarmensagem(string message)
        {
            MessageBox.Show(message, "produto salvo", MessageBoxButtons.OK);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                fluxodecaixaDTO roni = new fluxodecaixaDTO();
                roni.data = txtdata.Text;
                roni.ganhos = Convert.ToInt32(txtganhos.Text);
                roni.despesas = Convert.ToInt32(txtdespesas.Text);
                fluxodecaixaBUSINESS business = new fluxodecaixaBUSINESS();
                business.salvar(roni);
                enviarmensagem("Dados Cadastrados!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("erro" + ex.Message);
                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }

        public void LoadGrid()
        {
            try
            {
                string data = txtdata.Text;
                int ganhos = Convert.ToInt32(txtganhos.Text);
                int despesas = Convert.ToInt32(txtdespesas.Text);
                fluxodecaixaBUSINESS business = new fluxodecaixaBUSINESS();
                List<fluxodecaixaDTO> does = business.consultar(data, ganhos, despesas);
                gvfluxodecaixa.AutoGenerateColumns = false;
                gvfluxodecaixa.DataSource = does;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro! " + ex.Message);
                
            }
            


        }
    }
}
