﻿using progamin_chatu.Conexão.Classes_Principais.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Atualizado
{
    public partial class ConsultarFolha : Form
    {
        public ConsultarFolha()
        {
            InitializeComponent();
        }

        private void ConsultarFolha_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarFP();
        }

        public void CarregarFP()
        {
            try
            {
                string nome = txtnome.Text;
                string salariobase = txtcpf.Text;
                FolhaBusiness business = new FolhaBusiness();
                List<FolhaDTO> pag = business.Consultar(nome, salariobase);

                gvFolhaDePag.AutoGenerateColumns = false;
                gvFolhaDePag.DataSource = pag;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
                
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //           try
            //            {
            //                string nome = txtnome.Text;
            //                string cpf = txtcpf.Text;
            //                FolhaBusiness business = new FolhaBusiness();
            //                List<FolhaDTO> pag = business.Remover(id);
            //
            //                gvFolhaDePag.AutoGenerateColumns = false;
            //                gvFolhaDePag.DataSource = pag;
            //            }
            //            catch (Exception ex)
            //            {
            //                MessageBox.Show("Erro " + ex.Message);
            //            }
            //        }
        }
    }
}
