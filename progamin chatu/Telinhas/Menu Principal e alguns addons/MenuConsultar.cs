﻿using progamin_chatu.Telinhas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace progamin_chatu.Atualizado
{
    public partial class MenuConsultar : Form
    {
        public MenuConsultar()
        {
            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]

        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void button3_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new Consultar());
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Principal tela = new Principal();
            tela.Show();
            Hide();
        }


        private void AbrirFormInPanel(object formHijo)
        {
            try
            {
                if (this.panelcentral.Controls.Count > 0)
                    this.panelcentral.Controls.RemoveAt(0);
                Form fh = formHijo as Form;
                fh.TopLevel = false;
                fh.FormBorderStyle = FormBorderStyle.None;
                fh.Dock = DockStyle.Fill;
                this.panelcentral.Controls.Add(fh);
                this.panelcentral.Tag = fh;
                fh.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro!: " + ex.Message);               
            }
            
        }


        private void BtnConsultar_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new ConsultarFuncionario());
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void panel2_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint_2(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            AbrirFormInPanel(new ConsultarFolha());
        }
    }


}

    

