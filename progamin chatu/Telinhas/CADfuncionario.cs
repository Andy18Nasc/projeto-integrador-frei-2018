﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class CADfuncionario : Form
    {
        public CADfuncionario()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Principal tela = new Principal();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DtoFuncionario cad = new DtoFuncionario();
                cad.nome = txtnome.Text;
                cad.cargo = txtcargo.Text;

                BusinessFuncionario business = new BusinessFuncionario();
                business.Salvar(cad);

                EnviarMensagem("Cadastro feito com sucesso!");
            }
            catch (ArgumentException ex)
            {
                EnviarMensagemErro(ex.Message);

            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro: " + ex.Message);
            }
        }



        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "GRstore",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "GRstore",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
    }
}

